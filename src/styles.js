import { Appearance, useColorScheme } from 'react-native-appearance'
import { Dimensions } from './components'

const { height: screenHeight } = Dimensions.get('window')
const isDark = Appearance.getColorScheme() === 'dark'

const backgroundColor = isDark ? '#000000' : '#ffffff'
const buttonBackgroundColor = isDark ? '#663366' : '#ffccff'
const fontColor = isDark ? '#ffffff' : '#000000'

export const addStyles = {
  input: {
    borderColor: fontColor,
    borderRadius: 10,
    borderStyle: 'solid',
    borderWidth: 1,
    color: fontColor,
    padding: 10,
  },
  inputContainer: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  label: {
    color: fontColor,
  },
  labelHolder: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 5,
    paddingTop: 5,
  },
  page: {
    // For when the keyboard is open
    paddingBottom: 350,
  },
  submitButton: {
    backgroundColor: '#77cccc',
    padding: 10,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
  },
  testButton: {
    backgroundColor: '#ff9977',
    padding: 4,
  },
}

export const appStyles = {
  button: {
    backgroundColor: buttonBackgroundColor,
    borderRadius: 20,
    color: fontColor,
    padding: 10,
    marginLeft: 5,
    marginRight: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 15,
  },
  container: {
    backgroundColor,
    height: '100%',
    paddingTop: 70,
  },
}

export const mainStyles = {
  closeButton: {
    borderColor: '#cccccc',
    borderRadius: 20,
    borderStyle: 'solid',
    borderWidth: 1,
    color: fontColor,
    padding: 5,
    position: 'absolute',
    textAlign: 'center',
    top: -40,
    width: '100%',
    zIndex: 30,
  },
  site: {
    borderColor: '#333333',
    borderStyle: 'solid',
    borderWidth: 2,
    height: 150,
    marginLeft: 5,
    marginRight: 5,
    padding: 25,
    width: 150,
  },
  siteContainer: {
    flexDirection: 'row',
  },
  siteName: {
    color: '#336699',
    fontSize: 20,
  },
  siteLastUpdated: {
    color: fontColor,
  },
  siteUnread: {
    backgroundColor: '#ee5555',
    borderRadius: 20,
    height: 15,
    position: 'absolute',
    right: 15,
    top: 15,
    width: 15,
  },
  webview: {
    height: '100%',
    position: 'absolute',
    width: '100%',
  },
  webviewContainer: {
    height: screenHeight - 70,
    position: 'absolute',
    width: '100%',
  },
}
