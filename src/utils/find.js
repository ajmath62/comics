export default function find(node, sel) {
  if (isMatch(node, sel)) return node
  else if (!node.childNodes) return null
  else {
    // TODO: find *any* other way of doing this
    for (let i = 0; i < node.childNodes.length; i ++) {
      const n = node.childNodes[i]
      // Recursive call to find
      const found = find(n, sel)
      if (found) return found
    }
    return null
  }
}

function isMatch(node, [selectorArray]) {
  const nodeAttrs = node.attrs || []
  const attrs = nodeAttrs.reduce((obj, { name, value }) => ({...obj, [name]: value}), {})
  return selectorArray.every(sel => {
    if (sel === null) return true // universal selector
    const [type, ...content] = sel
    if (type === 'type') {
      const [elementType] = content
      return elementType === node.nodeName
    } else if (type === 'attr') {
      const [attr, matchType, matchValue] = content
      const nodeValue = attrs[attr] || ''
      if (matchType === undefined) {
        // Presence of attribute
        // Has to use attrs[attr] because nodeValue is always a string
        return attrs[attr] !== undefined
      } else if (matchType === 'exact') {
        return nodeValue === matchValue
      } else if (matchType === 'subcode') {
        return nodeValue === matchValue || nodeValue.startsWith(`${matchValue}-`)
      } else if (matchType === 'word') {
        const words = nodeValue.split(/\s+/)
        return words.indexOf(matchValue) !== -1
      } else if (matchType === 'start') {
        return nodeValue.startsWith(matchValue)
      } else if (matchType === 'end') {
        return nodeValue.endsWith(matchValue)
      } else if (matchType === 'contains') {
        return nodeValue.indexOf(matchValue) !== -1
      }
    } else if (type === 'index') {
      const [indexType, indexStr] = content
      const index = Number(indexStr)
      const siblingNodes = node.parentNode.childNodes.filter(n => n.nodeName !== '#text')
      if (indexType === 'nth-child') {
        return index === siblingNodes.indexOf(node) + 1
      } else if (indexType === 'nth-of-type') {
        const siblingsOfType = siblingNodes.filter(n => n.nodeName === node.nodeName)
        return index === siblingsOfType.indexOf(node) + 1
      } else if (indexType === 'nth-last-child') {
        return index === siblingNodes.length - siblingNodes.indexOf(node)
      } else if (indexType === 'nth-last-of-type') {
        const siblingsOfType = siblingNodes.filter(n => n.nodeName === node.nodeName)
        return index === siblingsOfType.length - siblingsOfType.indexOf(node)
      }
    } else if (type === 'empty') {
      return node.childNodes.length === 0
    } else if (type === 'contains') {
      const [matchText] = content
      const nodeText = node.childNodes.map(node => node.nodeName === '#text' ? node.value : '').join('')
      return matchText === nodeText
    }
  })
}
