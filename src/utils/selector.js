export class InvalidSelectorError extends Error {}

const valueTypeMapping = {
  '': 'exact',
  '|': 'subcode',
  '~': 'word',
  '^': 'start',
  '$': 'end',
  '*': 'contains',
}
// TODO: allow combinators
const combinators = [
  / +/, // descendant
  / *> */, // child
  / *~ */, // sibling
  / *\+ */, // immediate sibling
]
const simpleSelectors = [
  // TODO: distinguish these two as `start only`
  [/^\*/, () => null], // universal
  [/^[\w-]+/, ([text]) => ['type', text]], // type
  [/^\[([\w-]+)]/, ([, attr]) => ['attr', attr]], // attr present
  [
    /^\[([\w-]+)([|~^$*]?)=([^\]]+)]/,
    ([, attr, valueType, value]) => ['attr', attr, valueTypeMapping[valueType], value]
  ], // attr value
  [/^\.([\w-]+)/, ([, value]) => ['attr', 'class', 'word', value]], // class
  [/^#([\w-]+)/, ([, value]) => ['attr', 'id', 'exact', value]], // id
  [
    /^:(nth-child|nth-last-child|nth-of-type|nth-last-of-type)\((\d+)\)/,
    ([, type, index]) => ['index', type, index]
  ], // index
  [/^:empty/, () => ['empty']],
  [/^:contains\(([^)]+)\)/, ([, text]) => ['contains', text]], // text
]

export function makeSelectorObject(cssSelector) {
  let remainingString = cssSelector
  const selectorObject = []
  while (remainingString) {
    let found = false
    for (const [regex, callback] of simpleSelectors) {
      const match = regex.exec(remainingString)
      if (match) {
        selectorObject.push(callback(match))
        remainingString = remainingString.substr(match[0].length)
        found = true
        break  // exit the for loop
      }
    }
    if (!found) {
      throw new InvalidSelectorError(`No match found at '${remainingString.substr(0, 10)}'`)
    }
  }
  // Encase this in a list for down the road when we add combinators (and therefore have a list of selectors)
  return [selectorObject]
}
