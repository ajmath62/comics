import React from 'react'
import { parse } from 'parse5'
import { sortBy } from 'lodash'

import { Alert, AsyncStorage, Clipboard, ScrollView, Text, View } from './components'
import AddSite from './components/AddSite'
import Main from './components/Main'
import find from './utils/find'
import { appStyles } from './styles'

/*
Site {
  doneSel: optional object,
  lastUpdated: optional Date,
  loading: optional boolean,
  name: string,
  nextSel: object,
  read: boolean,
  url: string,
}
*/

const storagePrefix = 'site-'

export default class App extends React.Component {
  state = { sites: [], view: 'main' }

  componentDidMount() {
    // noinspection JSIgnoredPromiseFromCall
    this.refreshSites()
  }

  addSite = site => {
    AsyncStorage.setItem(`${storagePrefix}${site.name}`, JSON.stringify(site))
    // The site is assumed unread, so there is no need to load anything new
    this.updateSite(site, false)
    this.setState({ view: 'main' })
  }

  checkForNew = async site => {
    const { doneSel, nextSel, read, url } = site

    // The user hasn't read the latest URL
    if (!read) {
      this.updateSite(site)
      return
    }

    // Get the latest URL and look for a newer URL
    let response
    try {
      response = await fetch(url)
    } catch (e) {
      // We hit a CORS issue running in browser, so try hitting the local proxy server if any
      response = await fetch(`http://localhost:5000/${url}`)
    }
    const content = await response.text()
    const data = parse(content)

    // Look for the element signifying no new content
    if (doneSel) {
      const doneElem = find(data, doneSel)
      if (doneElem) {
        this.updateSite(site)
        return
      }
    }
    // Look for the element signifying new content
    const nextElem = find(data, nextSel)
    if (!nextElem) {
      this.updateSite(site)
      return
    }

    // Generate, save and return the new content URL
    const next = nextElem.attrs.find(({ name }) => name === 'href').value
    let nextUrl
    if (next.substr(0, 4) === 'http') {
      // Absolute URL
      nextUrl = next
    } else {
      // Relative URL
      const baseUrl = /https?:\/\/[^/]+/.exec(url)
      const nextSlash = next.substr(0, 1) === '/' ? next : `/${next}`
      nextUrl = `${baseUrl}${nextSlash}`
    }
    site.url = nextUrl
    site.read = false
    site.lastUpdated = new Date()
    this.updateSite(site)
  }

  refreshSites = async () => {
    const existingSites = await AsyncStorage.getAllKeys()
    existingSites.forEach(async key => {
      if (!key.startsWith(storagePrefix)) {
        return
      }
      const site = JSON.parse(await AsyncStorage.getItem(key))
      // Convert the lastUpdated timestamp from a string to a Date
      if (site.lastUpdated) site.lastUpdated = new Date(site.lastUpdated)
      this.updateSite(site, true)
      await this.checkForNew(site)
    })
  }

  exportSites = async () => {
    const sites = await AsyncStorage.getAllKeys()
    const siteData = (await Promise.all(sites.map(async key => {
      if (key.startsWith(storagePrefix)) {
        const site = JSON.parse(await AsyncStorage.getItem(key))
        // Get rid of all other keys
        // I prefer this key order for fixtures
        return {
          name: site.name,
          url: site.url,
          doneSelStr: site._doneSelString,
          nextSelStr: site._nextSelString,
        }
      } else {
        return null
      }
    }))).filter(s => s !== null)
    const sortedData = sortBy(siteData, s => s.name)
    await Clipboard.setString(JSON.stringify(sortedData, null, 2))
    Alert.alert('The sites have been copied to your clipboard')
  }

  updateSite = (site, isLoading) => {
    // lastUpdated should never be undefined in state (though it is allowed to be in storage)
    if (!site.lastUpdated) site.lastUpdated = new Date()
    // loading should never be in storage
    site.loading = undefined

    // Update the site in storage
    // These two operations can happen in parallel
    AsyncStorage.setItem(`${storagePrefix}${site.name}`, JSON.stringify(site))

    // Update the site in state
    site.loading = isLoading ? true : undefined
    this.setState(state => ({
      sites: { ...state.sites, [site.name]: site },
    }))
  }

  render() {
    const { sites, view } = this.state
    // Sort the sites by unread first and latest updated first
    const sortedSites = sortBy(Object.values(sites), [s => s.read || false, s => -s.lastUpdated])
    return (
      <ScrollView style={appStyles.container}>
        <View style={appStyles.buttonContainer}>
          <Text onPress={this.exportSites} style={appStyles.button}>Export Sites</Text>
          <Text onPress={this.refreshSites} style={appStyles.button}>Update</Text>
          {view === 'main'
            ? <Text onPress={() => this.setState({ view: 'add' })} style={appStyles.button}>Add Site</Text>
            : <Text onPress={() => this.setState({ view: 'main' })} style={appStyles.button}>Back</Text>
          }
        </View>
        {view === 'main'
          ? <Main
            checkForNew={this.checkForNew}
            sites={sortedSites}
            updateSite={this.updateSite}
          />
          : <AddSite addSite={this.addSite} />
        }
      </ScrollView>
    )
  }
}

