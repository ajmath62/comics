try {
  // Running on react native
  module.exports = require('react-native')
} catch(e) {
  // Running in browser
  module.exports = require('./components.browser')
}
