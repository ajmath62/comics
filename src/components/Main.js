import React from 'react'

import { ScrollView, Text, TouchableOpacity, View, WebView } from '../components'
import { mainStyles } from '../styles'

const dateFormat = {
  day: 'numeric',
  hour: 'numeric',
  month: 'short',
}

export default class Main extends React.Component {
  state = { active: null }

  close = () => {
    this.setState({ active: null })
  }

  view = site => {
    const { checkForNew, updateSite } = this.props
    site.read = true
    this.setState({ active: site.url })
    updateSite(site)
    // noinspection JSIgnoredPromiseFromCall
    checkForNew(site)
  }

  render() {
    const { sites } = this.props
    const { active } = this.state
    return [
      <ScrollView horizontal key='sites' style={mainStyles.siteContainer}>
        {sites.map(site => site.loading ? (
          <View key={site.name} style={mainStyles.site}>
            <Text style={mainStyles.siteName}>{site.name}</Text>
            <Text style={mainStyles.siteLastUpdated}>Loading...</Text>
          </View>
        ) : (
          <TouchableOpacity key={site.name} onPress={() => this.view(site)} style={mainStyles.site}>
            <Text style={mainStyles.siteName}>{site.name}</Text>
            <Text style={mainStyles.siteLastUpdated}>{site.lastUpdated.toLocaleString(undefined, dateFormat)}</Text>
            {site.read ? null : <View style={mainStyles.siteUnread} />}
          </TouchableOpacity>
        ))}
      </ScrollView>,
      active ? (
        <View key='webview' style={mainStyles.webviewContainer}>
          <WebView
            source={{ uri: active }}
            style={mainStyles.webview}
            useWebKit
          />
          <Text onPress={this.close} style={mainStyles.closeButton}>Close</Text>
        </View>
      ) : null,
    ]
  }
}
