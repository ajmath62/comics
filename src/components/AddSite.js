import React from 'react'
import { parse } from 'parse5'

import { Alert, Text, TextInput, View } from '../components'
import { makeSelectorObject } from '../utils/selector'
import find from '../utils/find'
import { addStyles } from '../styles'

class ValidationError extends Error {}

export default class AddSite extends React.Component {
  state = {
    doneSel: '',
    name: '',
    nextSel: '',
    url: '',
  }

  submit = () => {
    const { addSite } = this.props
    let newSite
    try {
      newSite = this.validate()
    } catch (e) {
      if (e instanceof ValidationError) {
        Alert.alert('Invalid data', e.message)
        return
      } else {
        throw e
      }
    }
    addSite(newSite)
  }

  test = async selector => {
    /** Test the provided selector against the specified URL */
    const { url } = this.state

    let selObj
    try {
      selObj = makeSelectorObject(selector)
    } catch (e) {
      Alert.alert('Invalid selector', e.message)
      return
    }

    let response
    try {
      response = await fetch(url)
    } catch (e) {
      // We hit a CORS issue running in browser, so try hitting the local proxy server if any
      response = await fetch(`http://localhost:5000/${url}`)
    }
    const content = await response.text()
    const data = parse(content)

    const elem = find(data, selObj)
    if (elem) {
      const attrs = elem.attrs || []
      Alert.alert(
        'Matching element found:',
        `<${elem.nodeName} ${attrs.map(({ name, value }) => `${name}="${value}"`).join(' ')}>`
      )
    } else {
      Alert.alert('No matching element found')
    }
  }

  testDone = async () => {
    const { doneSel } = this.state
    await this.test(doneSel)
  }

  testNext = async () => {
    const { nextSel } = this.state
    await this.test(nextSel)
  }

  validate = () => {
    const { doneSel, name, nextSel, url } = this.state
    if (!name) throw new ValidationError('You must specify a name')
    if (!nextSel) throw new ValidationError('You must specify a `next` selector')
    if (!url) throw new ValidationError('You must specify a URL')
    let doneSelObj = undefined
    let nextSelObj
    try {
      nextSelObj = makeSelectorObject(nextSel)
    } catch (e) {
      throw new ValidationError(`Invalid \`next\` selector: ${e.message}`)
    }
    if (doneSel) {
      try {
        doneSelObj = makeSelectorObject(doneSel)
      } catch (e) {
        throw new ValidationError(`Invalid \`done\` selector: ${e.message}`)
      }
    }

    return {
      // Keep the originals for later human viewing and re-entry
      _doneSelString: doneSel || undefined,
      _nextSelString: nextSel,
      doneSel: doneSelObj,
      name,
      nextSel: nextSelObj,
      url,
    }
  }

  render() {
    const { doneSel, name, nextSel, url } = this.state
    return (
      <View style={addStyles.page}>
        <View style={addStyles.inputContainer}>
          <View style={addStyles.labelHolder}>
            <Text style={addStyles.label}>Name</Text>
          </View>
          <TextInput
            name='name'
            onChangeText={text => this.setState({ name: text })}
            style={addStyles.input}
            value={name}
          />
        </View>
        <View style={addStyles.inputContainer}>
          <View style={addStyles.labelHolder}>
            <Text style={addStyles.label}>URL</Text>
          </View>
          <TextInput
            name='url'
            onChangeText={text => this.setState({ url: text })}
            style={addStyles.input}
            value={url}
          />
        </View>
        <View style={addStyles.inputContainer}>
          <View style={addStyles.labelHolder}>
            <Text style={addStyles.label}>Selector for Next page</Text>
            <Text onPress={this.testNext} style={addStyles.testButton}>Test</Text>
          </View>
          <TextInput
            name='nextSel'
            onChangeText={text => this.setState({ nextSel: text })}
            style={addStyles.input}
            value={nextSel}
          />
        </View>
        <View style={addStyles.inputContainer}>
          <View style={addStyles.labelHolder}>
            <Text style={addStyles.label}>Selector for Done (optional)</Text>
            <Text onPress={this.testDone} style={addStyles.testButton}>Test</Text>
          </View>
          <TextInput
            name='doneSel'
            onChangeText={text => this.setState({ doneSel: text })}
            style={addStyles.input}
            value={doneSel}
          />
        </View>
        <Text onPress={this.submit} style={addStyles.submitButton}>Submit</Text>
      </View>
    )
  }
}
