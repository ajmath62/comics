import React from 'react'

// Style present on all react native elements unless overridden
const commonStyle = {
  display: 'flex',
  flexDirection: 'column',
  position: 'relative',
}

export class Alert {
  static alert(title, message='') {
    window.alert(`${title}\n${message}`)
  }
}

export class AsyncStorage {
  static async getItem(key) {
    return sessionStorage.getItem(key)
  }

  static async setItem(key, value) {
    sessionStorage.setItem(key, value)
  }

  static async removeItem(key) {
    sessionStorage.removeItem(key)
  }

  static async getAllKeys() {
    return Object.keys(sessionStorage)
  }
}

export class Clipboard {
  // TODO: this does not have a perfect analogue in a browser
  static async getString() {
    console.warn('Not yet implemented')
  }

  static async setString(value) {
    console.log('Logging to clipboard:')
    console.log(value)
  }
}

export class Dimensions {
  static get(dim) {
    if (dim === 'window') {
      return {
        height: window.innerHeight,
        width: window.innerWidth,
      }
    } else {
      throw Error(`No dimension set for key ${dim}`)
    }
  }
}

export class ScrollView extends React.Component {
  render() {
    const { horizontal, style, ...props } = this.props
    const overflowDirection = horizontal ? 'overflowX' : 'overflowY'
    return <div style={{ [overflowDirection]: 'scroll', ...commonStyle, ...style }} {...props} />
  }
}

export class Text extends React.Component {
  render() {
    const { onPress, style, ...props } = this.props
    if (onPress) {
      return <button onClick={onPress} style={{ ...commonStyle, ...style }} {...props} />
    } else {
      return <span style={{ ...commonStyle, ...style}} {...props} />
    }
  }
}

export class TextInput extends React.Component {
  render() {
    const { onChangeText, ...props } = this.props
    return <input onChange={event => onChangeText(event.target.value)} type='text' {...props}/>
  }
}

export class TouchableOpacity extends React.Component {
  render() {
    const { onPress, style, ...props } = this.props
    return <div onClick={onPress} style={{ ...commonStyle, ...style }} {...props} />
  }
}

export class View extends React.Component {
  render() {
    const { onPress, style, ...props } = this.props
    return <div onClick={onPress} style={{ ...commonStyle, ...style }} {...props} />
  }
}

export class WebView extends React.Component {
  render() {
    const { source, ...props } = this.props
    return <iframe src={source.uri} {...props} />
  }
}
