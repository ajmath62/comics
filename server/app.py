import requests
from flask import Flask, Response


app = Flask(__name__)

@app.route('/<path:path>')
def hello(path):
    try:
        content = requests.get(path).content
    except requests.exceptions.MissingSchema:
        return '', 404

    response = Response(content)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response
